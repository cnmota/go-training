package main

import (
	"fmt"

	"gitlab.com/cnmota/go-training/01_domain/domain"
)

func main() {
	var (
		temp = domain.Temperature{
			Value: 10,
			Scale: domain.Celsius,
		}
	)

	city := domain.City{
		name: "London",
	}

	fmt.Println(temp)
}
