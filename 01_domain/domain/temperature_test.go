package domain

import (
	"testing"
)

func TestNewTemperature(t *testing.T) {
	_, err := NewTemperature(10, 32)

	if err == nil {
		t.Error("Temperature with an invalid scale should return an error")
	}

	temp, err := NewTemperature(10, Celsius)

	if err != nil {
		t.Error("Temperature with valid params should not return an error")
	}

	if temp.Value != 10 {
		t.Error("Did not set the correct value on temperature")
	}

	if temp.Scale != Celsius {
		t.Error("Did not set the correct value on temperature")
	}
}

func TestConversion(t *testing.T) {
	tc, err := NewTemperature(20, Celsius)
	if err != nil {
		t.Error("Temperature with valid params should not return an error")
	}

	tf, err := NewTemperature(68, Fahrenheit)
	if err != nil {
		t.Error("Temperature with valid params should not return an error")
	}

	if tc.Celsius() != 20 {
		t.Error("Celsius temperature conversion is not correct")
	}

	if tc.Fahrenheit() != 68 {
		t.Error("Fahrenheit temperature conversion is not correct")
	}

	if tf.Celsius() != 20 {
		t.Error("Celsius temperature conversion is not correct")
	}

	if tf.Fahrenheit() != 68 {
		t.Error("Fahrenheit temperature conversion is not correct")
	}
}
