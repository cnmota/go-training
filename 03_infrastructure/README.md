## Infrastucture

To continue the project instead of working with mocks we will now need to start and create implementations, of the defined interfaces for the storage and gateway

* The Gateway is a very simple type that calls the openweather api and translates the response into our domain model.
* The Repository is an in-memory map that will store and cache all results so when the Gateway fails we can use that fallback.

This code exemplifies JSON decoding, the use of maps, and introduces us to the concept of mutex to avoid concurrent access to shared data.
