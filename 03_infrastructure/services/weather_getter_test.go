package services

import (
	"errors"
	"testing"

	. "github.com/onsi/gomega"

	"gitlab.com/cnmota/go-training/03_infrastructure/domain"
	"gitlab.com/cnmota/go-training/03_infrastructure/tests/services/mocks"
)

var (
	city = domain.City{}
)

func TestTemperatureForWhenGatewayAndRepositoryFails(t *testing.T) {
	RegisterTestingT(t)

	repository := mocks.CityRepository{}
	gateway := mocks.WeatherGateway{}

	announcer := Announcer{&gateway, &repository}

	gateway.On("TemperatureFor", "Lisbon").Return(domain.Temperature{}, errors.New("boom"))
	repository.On("CityWith", "Lisbon").Return(domain.City{}, errors.New("boom"))

	temp, err := announcer.TemperatureFor("Lisbon")

	Expect(err).To(HaveOccurred(), "should return an error, when gateway fails and repository fails")
	Expect(temp).To(Equal(float64(0)), "should return 0 temperature")
}

func TestTemperatureForWhenGatewayFailsAndRepositoryReturnsCachedValue(t *testing.T) {
	RegisterTestingT(t)

	repository := mocks.CityRepository{}
	gateway := mocks.WeatherGateway{}

	announcer := Announcer{&gateway, &repository}

	repository.On("CityWith", "Lisbon").Return(
		domain.NewCity(
			"Lisbon", domain.Temperature{Value: 50, Scale: domain.Fahrenheit},
		),
		nil,
	)

	temp, err := announcer.TemperatureFor("Lisbon")

	Expect(temp).To(Equal(float64(10)), "should have returned the expected value")
	Expect(err).ToNot(HaveOccurred(), "should not return an error, when gateway fails and repository returns value")
}

func TestTemperatureForWhenGatewayReturnsButFailsToStore(t *testing.T) {
	RegisterTestingT(t)

	repository := mocks.CityRepository{}
	gateway := mocks.WeatherGateway{}

	announcer := Announcer{&gateway, &repository}

	repository.On("CityWith", "Lisbon").Return(domain.City{}, errors.New("failed"))
	gateway.On("TemperatureFor", "Lisbon").Return(domain.Temperature{Value: 20, Scale: domain.Celsius}, nil)
	repository.On("StoreCity", domain.NewCity("Lisbon", domain.Temperature{Value: 20, Scale: domain.Celsius})).Return(errors.New("boom"))

	temp, err := announcer.TemperatureFor("Lisbon")

	Expect(temp).To(Equal(float64(20)), "should have returned the expected value")
	Expect(err).ToNot(HaveOccurred(), "should not return an error, when gateway fails and repository returns value")
}

func TestTemperatureForHappyPath(t *testing.T) {
	RegisterTestingT(t)

	repository := mocks.CityRepository{}
	gateway := mocks.WeatherGateway{}

	announcer := Announcer{&gateway, &repository}

	repository.On("CityWith", "Lisbon").Return(domain.City{}, errors.New("failed"))
	gateway.On("TemperatureFor", "Lisbon").Return(domain.Temperature{Value: 20, Scale: domain.Celsius}, nil)
	repository.On("StoreCity", domain.NewCity("Lisbon", domain.Temperature{Value: 20, Scale: domain.Celsius})).Return(nil)

	temp, err := announcer.TemperatureFor("Lisbon")

	Expect(temp).To(Equal(float64(20)), "should have returned the expected value")
	Expect(err).ToNot(HaveOccurred(), "should not return an error, when gateway fails and repository returns value")
}
