package infrastructure

import (
	"testing"

	"gitlab.com/cnmota/go-training/03_infrastructure/domain"

	. "github.com/onsi/gomega"
)

//api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=002fcb7204f9cfc4dee1a4b1f53f35de

func TestTemperatureFor(t *testing.T) {
	RegisterTestingT(t)

	gw := NewGateway("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s", "002fcb7204f9cfc4dee1a4b1f53f35de")

	temp, err := gw.TemperatureFor("Oporto")

	Expect(err).To(BeNil(), "should not return an error")
	Expect(temp.Scale).To(Equal(domain.Fahrenheit), "should match scale")
	Expect(temp.Value).ToNot(Equal(0), "should match scale")
}
