package infrastructure

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/cnmota/go-training/03_infrastructure/domain"
)

//api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=002fcb7204f9cfc4dee1a4b1f53f35de

// WeatherGateway ...
type WeatherGateway struct {
	client   *http.Client
	endpoint string
	key      string
}

type weatherResponse struct {
	Main struct {
		Temperature float64 `json:"temp"`
	} `json:"main"`
}

// NewGateway creates a new gateway
func NewGateway(endpoint, apikey string) WeatherGateway {
	return WeatherGateway{
		endpoint: endpoint,
		key:      apikey,
		client: &http.Client{
			Timeout: 2 * time.Second,
		},
	}
}

// TemperatureFor implements gateway
func (g *WeatherGateway) TemperatureFor(city string) (domain.Temperature, error) {
	var (
		temperature domain.Temperature
		url         = fmt.Sprintf(g.endpoint, city, g.key)
	)

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return temperature, err
	}

	response, err := g.client.Do(request)
	if err != nil {
		return temperature, err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return temperature, err
	}

	wResponse := weatherResponse{}
	if err := json.Unmarshal(body, &wResponse); err != nil {
		return temperature, err
	}

	return domain.NewTemperature(wResponse.Main.Temperature, domain.Fahrenheit)
}
