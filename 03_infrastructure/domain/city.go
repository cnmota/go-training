package domain

// City type to hold temperature data
// Why the lowercase instead of uppercase ??
type City struct {
	name        string
	temperature Temperature
}

// NewCity factory function to create a weather instance ...
func NewCity(city string, temp Temperature) City {
	return City{
		name:        city,
		temperature: temp,
	}
}

// City returns city value
func (c City) City() string {
	return c.name
}

// Temperature returns city temperature
func (c City) Temperature() Temperature {
	return c.temperature
}
