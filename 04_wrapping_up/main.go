package main

import (
	"fmt"
	"net/http"

	"gitlab.com/cnmota/go-training/04_wrapping_up/infrastructure"
	"gitlab.com/cnmota/go-training/04_wrapping_up/services"

	"github.com/labstack/echo"
)

type response struct {
	City        string  `json:"city"`
	Temperature float64 `json:"temp"`
}

func main() {
	gateway := infrastructure.NewGateway("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s&units=imperial", "002fcb7204f9cfc4dee1a4b1f53f35de")
	repository := infrastructure.NewRepository()

	service := services.NewAnouncer(&gateway, &repository)

	e := echo.New()
	//	e.Shutdown(context.Background())

	e.GET("/api/1/temperature/:cityName", func(e echo.Context) error {
		cityName := e.Param("cityName")
		if cityName == "" {
			return e.NoContent(http.StatusBadRequest)
		}

		temperature, err := service.TemperatureFor(cityName)
		if err != nil {
			return e.NoContent(http.StatusNotFound)
		}

		return e.JSON(http.StatusOK, response{
			City:        cityName,
			Temperature: temperature,
		})
	})

	e.HideBanner = true

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", 8080)))
}
