package infrastructure

import (
	"errors"
	"sync"
	"time"

	"gitlab.com/cnmota/go-training/04_wrapping_up/domain"
)

// Errors
var (
	ErrNoData = errors.New("no data available")
)

type expirableCity struct {
	domain.City
	CreatedAt time.Time
}

// CityRepository defines interface behaviour for our City Weather Storage
type CityRepository struct {
	storage map[string]expirableCity
	mu      sync.Mutex
}

// NewRepository factory method that returns an initialized In-Memory Repository
func NewRepository() CityRepository {
	return CityRepository{
		storage: map[string]expirableCity{},
		mu:      sync.Mutex{},
	}
}

// CityWith implements CityRepository interface ... why the need for pointer here ??? Mutex passed by value is copied
func (r *CityRepository) CityWith(name string) (domain.City, error) {
	r.mu.Lock()
	defer func() {
		r.mu.Unlock()
	}()

	city, ok := r.storage[name]
	if !ok {
		return domain.City{}, ErrNoData
	}

	if city.CreatedAt.Before(time.Now().Add(-1 * time.Hour)) {
		return domain.City{}, ErrNoData
	}

	return city.City, nil
}

// StoreCity implements CityRepository interface
func (r *CityRepository) StoreCity(city domain.City) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.storage[city.City()] = expirableCity{
		City:      city,
		CreatedAt: time.Now(),
	}

	return nil
}
