## Golang

### Instalar Go

A maneira mais simples de instalar Go consiste em, ir ao site da linguagem à secção de downloads:

[https://golang.org/dl/]

Fazer download do instalador para a plataforma desejada

### Definir a GOPATH

Go necessita da definição de uma variável de ambiente onde os projetos são instalados, essa váriavel de ambiente é a GOPATH.

Tipicamente o instalador define-a na vossa Home Directory e espera que tudo o que seja Go related esteja debaixo dessa pasta

exemplo:

`GOPATH=/Users/nunomota/go``

** Estrutura da GOPATH **

```
~/go » ls -la
total 24
drwxr-xr-x    9 nunomota  staff    288 Mar 15  2017 .
drwxr-xr-x+ 137 nunomota  staff   4384 Jul 10 23:09 ..
drwxr-xr-x   52 nunomota  staff   1664 Jul 10 14:24 bin
drwxr-xr-x    4 nunomota  staff    128 Jul 27  2017 pkg
drwxr-xr-x   20 nunomota  staff    640 Jun  6 17:22 src
```

* bin - binarios executáveis obtidos através de go get
* pkg - pasta auxiliar para gestão de depêndencias e binarios intermédios
* src - source code onde devem residir os vossos projetos

### Estrutura da GOPATH/src

Dentro da pasta GOPATH/src residirão todos os vossos projetos, tipicamente os mesmo devem ser organizados da seguinte forma

* <git path projeto 1>
* <git path projeto 2>
* <git path projeto N>

Exemplo:

```
src/gitlab.com/cnmota/go-training
src/gitlab.com/cnmota/go-project-wireframe
src/gitlab.com/cnmota/veritas

```

## O que iremos fazer

Em vez de uma maçadora conversa em que são mostrados slides a explicar o que aparece em 40 tuturias feitos em go vamos efetivamente tentar construir um projeto simples.

Um sistema simples que usa a API do OpenweatherMap faz fetch da temperatura para uma determinada cidade, a converte para Celsius e a expõe via uma API nossa.

Como a API da Openweathermap têm limitações em modo free, iremos ao mesmo tempo adicionar um micro-sistema de caching in memory para a dita cidade.
