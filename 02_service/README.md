## Services

We will now create ou service, for this we will require the instalation of two external packages for testing purposes:

* https://github.com/vektra/mockery
* https://onsi.github.io/gomega/

The first one we will install, calling:

`go get github.com/vektra/mockery/.../`

The second one via dep

for this we will also need to install the dep package:

* https://github.com/golang/dep

dep is a package managment tool for go and it can be installed with:

`go get -u github.com/golang/dep/cmd/dep`

To automate this process we will add all this processes on a Makefile