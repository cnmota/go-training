package services

import (
	"log"

	"gitlab.com/cnmota/go-training/02_service/domain"
)

// WeatherGateway defines interface behaviour for our Weather API Gateway
type WeatherGateway interface {
	TemperatureFor(city string) (domain.Temperature, error)
}

// CityRepository defines interface behaviour for our City Weather Storage
type CityRepository interface {
	CityWith(name string) (domain.City, error)
	StoreCity(domain.City) error
}

// Announcer based on weather announcer, it the service that announces weather for a city
type Announcer struct {
	gateway    WeatherGateway
	repository CityRepository
}

// TemperatureFor returns the current or latest recorded temperature for a city
func (a Announcer) TemperatureFor(cityName string) (float64, error) {
	var (
		temp = domain.Temperature{}
		err  error
	)

	if city, err := a.repository.CityWith(cityName); err == nil {
		return city.Temperature().Celsius(), nil
	}

	temp, err = a.gateway.TemperatureFor(cityName)
	if err != nil {
		return 0, err
	}

	// We got a valid temperature let's store it on the repo when the gateway fails
	city := domain.NewCity(cityName, temp)

	if err := a.repository.StoreCity(city); err != nil {
		log.Println(err.Error())
	}

	return temp.Celsius(), nil
}

func (a Announcer) temperatureFromRepository(cityName string) (float64, error) {
	city, err := a.repository.CityWith(cityName)
	if err != nil {
		return 0, err
	}

	return city.Temperature().Celsius(), nil
}
