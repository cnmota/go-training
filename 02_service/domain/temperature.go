package domain

import "errors"

// Constants
const (
	Celsius    = 0
	Fahrenheit = 1
)

// Temperature type representing a temperature has scale and value
type Temperature struct {
	Value float64
	Scale int
}

// NewTemperature factory function to validate and create Temperature types
func NewTemperature(val float64, scale int) (Temperature, error) {
	if scale != Celsius && scale != Fahrenheit {
		return Temperature{}, errors.New("invalid scale for temperature")
	}

	return Temperature{val, scale}, nil
}

// Celsius returns Temperature value in Celsius
func (t Temperature) Celsius() float64 {
	if t.Scale == Celsius {
		return t.Value
	}

	return (t.Value - 32) / 1.8
}

// Fahrenheit returns Temperature value in Farenheit
func (t Temperature) Fahrenheit() float64 {
	if t.Scale == Fahrenheit {
		return t.Value
	}

	return t.Value*1.8 + 32
}
